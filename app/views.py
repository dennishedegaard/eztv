import datetime

from django.shortcuts import render_to_response, redirect
from django.utils import timezone

from eztv import get_cache, update_cache, TwitterError


def _add_created_css_class(data):
    '''
    Adds an element to the dicts in the list called 'created_class'.
    If 'created_class' is not None, it is a string with a css class.
    '''
    now = timezone.make_aware(datetime.datetime.now(),
                              timezone.get_default_timezone())
    one_day_ago = now - datetime.timedelta(1)
    two_days_ago = now - datetime.timedelta(2)

    for element in data:
        created = element['created']

        if created > one_day_ago:
            element['created_class'] = 'less_1_day'
        elif created < two_days_ago:
            element['created_class'] = 'more_2_days'


def index(request):
    '''
    Renders the index.html template, with the yield_data generator from the
    eztv-module.

    :param request: A django request object.
    :returns: A rendered django template in a HttpResponse object.
    '''
    try:
        data = get_cache()
    except TwitterError, e:
        error = e.message
        return render_to_response('index.html', dict(
            error=error,
        ))

    _add_created_css_class(data)

    return render_to_response('index.html', dict(
        data=data,
    ))


def update(request):
    '''
    Updates the cache and returns a redirect to /.

    :param request: A django request object.
    :returns: A redirect to '/' url.
    '''
    update_cache()
    return redirect('app.views.index')
