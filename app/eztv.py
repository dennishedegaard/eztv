'''
A library with methods for helping with getting the data for the EZTV project.

Since the project doesn't necessarily use any sort of datastorage, this might
as well count as the data-layer as well as model.
'''
import re
from contextlib import closing

import requests
from dateutil.parser import parse
from django.core.cache import cache

# The URL of the Twitter feed to listen on, in this case eztv_it.
TWITTER_URL = 'https://api.twitter.com/1/statuses/user_timeline.json' +\
              '?screen_name=eztv_it&count=200'

# Key of the cache.
CACHE_ENTRY_NAME = 'eztv_cache'
# Timeout of the cache (in seconds)
CACHE_ENTRY_TIMEOUT = 600

# This tuple if strings will, if matched be removed from the text.
# If matcher is a tuple, second element is a tag, as a string.
FILTERS = (
    'AAC',
    'AC3',
    'Bluray',
    'DVDRip',
    'WEBRip',
    ('HDTV', 'HDTV'),
    'PDTV',
    'XviD',
    ('720p', '720p'),
    (re.compile(r'[xX]264\-?\S*?'), 'x264'),
)

RE_TYPE = type(re.compile(''))


def _filter_text(text):
    '''
    Filters and tags the text based on FILTERS.

    :param text: The string to filter.
    :returns: Returns (text:str, tags:set). Tags may be an empty set.
    '''
    found = True
    text = text.strip()
    tags = set()

    while found:
        found = False

        for filter in FILTERS:
            tag = None
            text = text.strip()

            # Check if the filter is a tuple, with a tag.
            if isinstance(filter, tuple):
                filter, tag = filter

            # String matching.
            if isinstance(filter, str):
                if filter in text:
                    text = text.replace(filter, '')
                    found = True
            # Regex substitution.
            elif type(filter) == RE_TYPE:
                text, count = filter.subn('', text)
                if count > 0:
                    found = True
            # Unknown filter type.
            else:
                raise Exception(type(filter))

            # Handle tagging, if matched current filter.
            if found and tag is not None:
                tags.add(tag)

    return text, sorted(tags)


class TwitterError(Exception):
    '''
    If this message is thrown, it should contain a parsed message from a twitter
    response.
    '''
    def __init__(self, message):
        super(TwitterError, self).__init__(message)


def _get_twitter_data():
    '''
    Returns the data from the Twitter feed.

    If there's an error in the data returned, a TwitterError
    exception is thrown, with the first errormessage from the JSON.

    :returns: A dict containing all the data from the JSON.
    '''
    # Make a GET-request to the URL.
    with closing(requests.get(TWITTER_URL)) as conn:
        # Return the data decoded from JSON.
        data = conn.json()

        # Check for errors-element in the JSON.
        if 'errors' in data:
            raise TwitterError(data['errors'][0]['message'])

        return data


def yield_data():
    '''
    Yields dicts of data.

    If the result is not parsed, the output should not be trusted.

    Maybe throw a TwitterError, with the message from the response.

    :yields: Dict with parsed, created, text and url.
    '''
    re_text_url = re.compile('^(.+?)(\s?\[.*?\])?\s\-\s(http\:\/\/.+?)$')
    for row in _get_twitter_data():
        parsed = True
        tags = set()

        # Attempt to parse the date using the dateutil library.
        try:
            created = parse(row['created_at'])
        except ValueError, e:
            # Happens if the date parsing fails.
            created = e.message
            parsed = False

        # Attempt to parse the text, using a regex.
        match = re_text_url.match(row['text'])
        if match:
            output = match.groups(0)
            if len(output) == 2:  # If no [group] tag was found.
                text, url = output
            elif len(output) == 3:  # If a [group] tag was found, ignore it.
                text, _, url = output
            else:  # No idea what happened here.
                raise Exception(output)

            text, tags = _filter_text(str(text))

        else:
            # Unable to parse the text, just yield text, as is.
            text = row['text']
            url = 'Unable to parse'
            parsed = False

        yield dict(parsed=parsed, created=created,
                   text=text, url=url, tags=tags)


def update_cache():
    '''
    This method should be called when the cache is empty or outdated.

    It retreives new data and stores it in the cache.

    :returns: The newly created data, after it's stored in the cache.
    '''
    data = list(yield_data())

    cache.set(CACHE_ENTRY_NAME, data, CACHE_ENTRY_TIMEOUT)
    return data


def get_cache():
    '''
    Gets the current cached object and returns it.

    If the entry is empty or outdated, it is refreshed and then returned.

    :returns: The cached eztv object.
    '''
    _cache = cache.get(CACHE_ENTRY_NAME)

    # If cache is empty or outdated, update it.
    if _cache is None:
        _cache = update_cache()

    return _cache
