/*global $:false, moment:false */

$(function() {
    "use strict";

    /* Some initial moments used later. */
    var now_plus_1_day = moment().add('day', 1);
    var now_plus_2_days = moment().add('days', 2);

    /* Calculate the diff in time, to make it pretty :) */
    $('.created').each(function() {
        var _this = $(this);

        /* Parse the date from the django filter. */
        var _date = moment(_this.text(), 'YY-MM-DD HH:mm:ss');

        /* Replace date with delta time. */
        _this.text(_date.fromNow());
    });

    var find_elements_with_tag = function(tag) {
        return $('span.tags span.' + tag);
    };

    /* Tag mouseover effect. */
    $('span.tags span').hover(function() {
        var tag = $(this).attr('class');
        var elements = find_elements_with_tag(tag);
        elements.animate({
            opacity: 1.0
        }, 'fast');
    }, function() {
        var tag = $(this).attr('class');
        var elements = find_elements_with_tag(tag);
        elements.animate({
            opacity: .25
        }, 'fast');
    });
});
