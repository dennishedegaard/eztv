from django import template

register = template.Library()


@register.filter
def format_datetime(obj):
    return obj.strftime('%y-%m-%d %H:%M')
