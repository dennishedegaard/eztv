#!/usr/bin/env bash
# Bootstrap provision shell script for vagrant.

function start_debugserver {
    echo "Starting debug server in a screen session..."
    su - vagrant -c "screen -dm python /home/eztv/manage.py runserver 0.0.0.0:8000"
    if [ $? == 0 ]; then
        echo "Debug server listening on localhost:8000"
    else
        echo "Bad rc: $?"
    fi
}

if [ -e /root/.bootstrapped ]; then
    echo "Already bootstrapped..."
    start_debugserver
    exit 0
fi

echo "cd /home/eztv" >> ~vagrant/.profile

aptitude install -y python-pip memcached
pip install -r /home/eztv/requirements.txt

touch /root/.bootstrapped

start_debugserver

exit 0
